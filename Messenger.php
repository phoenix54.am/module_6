<?php


$textStorage = [];
function add(string $title, string $text, array &$storage): void
{
    $storage [] = [
        'title' => $title,
        'text' => $text,
    ];
}

function remove(int $index, array &$storage): bool
{
    if (isset($storage[$index])) {
        unset($storage[$index]);
        $sucsess = true;
    } else {$sucsess = false;}
    return $sucsess;
}

function edit(int $index, array &$storage, string $title = null, string $text = null): bool
{
    if (isset($storage[$index])) {
        if (isset($title)) {
            $storage[$index]['title'] = $title;
        }
        if (isset($text)) {
            $storage[$index]['text'] = $text;
        }
        return true;
   } 
   return false;
}

add('one', 'first',$textStorage);
add('two', 'second', $textStorage);
add('three', 'third', $textStorage);

var_dump($textStorage);

$a = remove(0,$textStorage);
var_dump($a);
$a = remove(2,$textStorage);
var_dump($a);
$a = remove(5,$textStorage);
var_dump($a);
var_dump($textStorage);

$a = edit(1,$textStorage,null,'dnoces');
var_dump($a);
var_dump($textStorage);
$a = edit(1,$textStorage,'null',null);
var_dump($a);
var_dump($textStorage);
$a = edit(0, $textStorage,'fooo','bar');
var_dump($a);
var_dump($textStorage);
